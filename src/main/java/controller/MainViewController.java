package controller;

import java.awt.event.ActionEvent;

import userInterface.TravelPlanner;
import view.MainView;
import view.PanelCiudades;
import view.PanelPlanificadores;
import view.PanelViaje;

public class MainViewController {
	
    private TravelPlanner model;
	private MainView      view;
	private PanelPlanificadores panelPlanificadores;
	private PanelCiudades       panelCiudades;
	private PanelViaje          panelViaje;

	public MainViewController(MainView view, TravelPlanner model) {
		this.model = model;
		this.view  = view;
		model.agregarObservador(view);
		inicializarPaneles();
	}
	
	private void inicializarPaneles() {
		panelCiudades = new PanelCiudades(model);
		panelPlanificadores = new PanelPlanificadores(model);
		panelViaje = new PanelViaje(model);
		panelPlanificadores.getBtnEnviar().addActionListener(this::enviarPlanificadorElegido);
		view.setPanelNorte(panelPlanificadores);
		view.setPanelOeste(panelCiudades);
		view.setPanelCentro(panelViaje);
	}
	
	private void enviarPlanificadorElegido(ActionEvent e) {
	    model.planificar(panelCiudades.getCiudadesSeleccionadas(),
	            panelPlanificadores.getPlanificadorSeleccionado(),
	            panelCiudades.getCiudadInicialSeleccionada());
	}
	
	public void inicializar() {
		this.view.getFrame().setVisible(true);
	}

}
