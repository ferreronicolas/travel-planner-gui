package main;

import java.awt.EventQueue;

import controller.MainViewController;
import userInterface.TravelPlanner;
import view.MainView;

public class Main {

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				TravelPlanner tp = new TravelPlannerMock();
				MainView mv = new MainView();
				MainViewController mvc = new MainViewController(mv, tp);
				mvc.inicializar();
			}
		});

	}

}
