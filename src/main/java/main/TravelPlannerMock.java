package main;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import userInterface.TravelPlanner;

public class TravelPlannerMock extends TravelPlanner {

    Map<String, Collection<String>> alojamientos;
    Map<String, Collection<String>> atracciones;
    Set<String> planificadores;

    public TravelPlannerMock() {
        alojamientos = new HashMap<String, Collection<String>>();
        atracciones  = new HashMap<String, Collection<String>>();
        Collection<String> alojamientosCiudad1 = new HashSet<String>();
        alojamientosCiudad1.add("alojamiento 1");
        alojamientosCiudad1.add("alojamiento 2");
        alojamientos.put("ciudad 1", alojamientosCiudad1);
        Collection<String> atraccionesCiudad1 = new HashSet<String>();
        atraccionesCiudad1.add("atraccion 1");
        atraccionesCiudad1.add("atraccion 2");
        atracciones.put("ciudad 1", atraccionesCiudad1);
        
        Collection<String> alojamientosCiudad2 = new HashSet<String>();
        alojamientosCiudad2.add("alojamiento 3");
        alojamientosCiudad2.add("alojamiento 4");
        alojamientos.put("ciudad 2", alojamientosCiudad2);
        Collection<String> atraccionesCiudad2 = new HashSet<String>();
        atraccionesCiudad2.add("atraccion 3");
        atraccionesCiudad2.add("atraccion 4");
        atracciones.put("ciudad 2", atraccionesCiudad2);
        
        
        planificadores = new HashSet<String>();
        planificadores.add("planificador menor costo");
        planificadores.add("planificador mas ciudades");
    }
    @Override
    public Collection<String> getCiudades() {
        return new HashSet<>(alojamientos.keySet());
    }
    
    @Override
    public Collection<String> getPlanificadores() {
        return new HashSet<>(planificadores);
    }
    
    @Override
    public Collection<String> getAtracciones(String ciudad) {
        return new HashSet<>(atracciones.get(ciudad));
    }
    
    @Override
    public Collection<String> getAlojamientos(String ciudad) {
        return new HashSet<>(alojamientos.get(ciudad));
    }
    
    @Override
    public String getPrecioAlojamiento(String ciudad, String alojamiento) {
        return "12";
    }
    
    @Override
    public String getPrecioAtraccion(String ciudad, String atraccion) {
        return "6";
    }
    
    @Override
    public void planificar(Collection<String> ciudades, String planificador, String ciudadInicial) {
        notificarObservadores();
    }
    
    @Override
    public List<String> getItinerario() {
        List<String> itinerario = new ArrayList<>();
        itinerario.add("ciudad 1");
        itinerario.add("ciudad 2");
        return itinerario;
    }
    
    @Override
    public String elegirAlojamiento(String ciudad) {
        if (ciudad.equals("ciudad 1"))
            return "alojamiento 1";
        return "alojamiento 3";
    }
    
    @Override
    public String elegirAtraccion(String ciudad) {
        if (ciudad.equals("ciudad 1"))
            return "atraccion 2";
        return "atraccion 3";
    }
    
    @Override
    public String elegirRuta(String ciudad1, String ciudad2) {
        return "ruta nacional 1";
    }
}
