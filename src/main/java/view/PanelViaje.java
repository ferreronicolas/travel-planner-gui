package view;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import userInterface.TravelPlanner;

public class PanelViaje extends JPanel {

    private static final long serialVersionUID = 1L;

    private TravelPlanner model;
    private List<String>  ciudades;
    private List<JLabel>  labels;

    public PanelViaje(TravelPlanner model) {
        this.model = model;
        ciudades   = new ArrayList<String>();
        labels     = new LinkedList<JLabel>();
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }
    
    private void actualizarComponentes() {
        removeAll();
        labels.clear();
        for (int i = 0; i < ciudades.size(); i++) {
            JLabel labelCiudad = new JLabel(String.valueOf(i+1) + ". " + ciudades.get(i));
            JLabel labelAlojamiento = new JLabel("Alojamiento elegido: " + model.elegirAlojamiento(ciudades.get(i)));
            JLabel labelAtraccion = new JLabel("Atracción elegida: " + model.elegirAtraccion(ciudades.get(i)));
            labelCiudad.setAlignmentX(CENTER_ALIGNMENT);
            labelAlojamiento.setAlignmentX(CENTER_ALIGNMENT);
            labelAtraccion.setAlignmentX(CENTER_ALIGNMENT);
            labels.add(labelCiudad);
            labels.add(labelAlojamiento);
            labels.add(labelAtraccion);
            if (i < ciudades.size() - 1) {
                JLabel labelRuta = new JLabel("--- " + model.elegirRuta(ciudades.get(i), ciudades.get(i+1)) + " ---");
                labelRuta.setAlignmentX(CENTER_ALIGNMENT);
                labels.add(labelRuta);
            }
        }
        labels.forEach(this::add);
        repaint();
    }

    public List<String> getCiudades() {
        return new ArrayList<String>(ciudades);
    }

    public void actualizarCiudades() {
        this.ciudades = model.getItinerario();
        actualizarComponentes();
    }

}
