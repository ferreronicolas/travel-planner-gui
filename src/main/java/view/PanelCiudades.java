package view;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.swing.BoxLayout;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import userInterface.TravelPlanner;

public class PanelCiudades extends JPanel {

	private static final long serialVersionUID = 1L;

	private Map<String, Collection<String>> alojamientos;
	private Map<String, Collection<String>> atracciones;
	private Collection<JCheckBox> checkBoxes;
	
	public PanelCiudades(TravelPlanner model) {
	    alojamientos = new HashMap<String, Collection<String>>();
	    atracciones  = new HashMap<String, Collection<String>>();
		for (String ciudad : model.getCiudades()) {
		    alojamientos.put(ciudad, model.getAlojamientos(ciudad));
		    atracciones.put(ciudad, model.getAtracciones(ciudad));
		}
		this.checkBoxes = new ArrayList<JCheckBox>();
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		actualizarComponentes();
	}
	
	private void actualizarComponentes() {
	    removeAll();
	    checkBoxes.clear();
	    alojamientos.keySet().forEach(c -> checkBoxes.add(new JCheckBox(c)));
	    checkBoxes.forEach(this::add);
	}

	public List<JCheckBox> getCheckBoxes() {
		return new ArrayList<JCheckBox>(checkBoxes);
	}

	public void setCheckBoxes(List<JCheckBox> checkBoxes) {
		this.checkBoxes = checkBoxes;
	}
	
	public Collection<String> getCiudadesSeleccionadas() {
	    return checkBoxes.stream()
	            .filter(JCheckBox::isSelected)
	            .map(JCheckBox::getText)
	            .collect(Collectors.toList());
	}

	public String getCiudadInicialSeleccionada() {
	    Object[] ciudadesSeleccionadas = getCiudadesSeleccionadas().toArray(new Object[0]);
	    String seleccion = (String) JOptionPane.showInputDialog(this,
	            "Seleccione una ciudad inicial para el viaje",
	            "Ciudad incial", 
	            JOptionPane.QUESTION_MESSAGE,
	            null,
	            ciudadesSeleccionadas,
	            ciudadesSeleccionadas[0]);
	    if (seleccion == null)
	        return "";
	    return seleccion;
	}
}
