package view;

import javax.swing.JLabel;
import javax.swing.JPanel;

public class PanelError extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private JLabel lblError;
	
	public PanelError() {
		inicializarComponentes();
	}
	
	private void inicializarComponentes() {
		lblError  = new JLabel("No se encontraron planificadores o ciudades.");
		add(lblError);
	}
	
	public JLabel lblError() {
		return lblError;
	}
	
	public void setLblError(JLabel label) {
		this.lblError = label;
	}

}
