package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import userInterface.Observer;

public class MainView implements Observer {

	private JFrame frame;
	
	private JPanel contentPane;
	private PanelPlanificadores panelNorte;
	private PanelCiudades       panelOeste;
	private PanelViaje          panelCentro;

	public MainView() {
		inicializarFrame();
	}
	
	private void inicializarFrame() {
		frame = new JFrame("Travel Planner");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		frame.setContentPane(contentPane);
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JPanel getContentPane() {
		return contentPane;
	}

	public void setContentPane(JPanel contentPane) {
		this.contentPane = contentPane;
	}

	public JPanel getPanelNorte() {
		return panelNorte;
	}

	public void setPanelNorte(PanelPlanificadores panelNorte) {
		this.panelNorte = panelNorte;
		this.frame.add(panelNorte, BorderLayout.NORTH);
	}

	public JPanel getPanelOeste() {
		return panelOeste;
	}

	public void setPanelOeste(PanelCiudades panelOeste) {
		this.panelOeste = panelOeste;
		this.frame.add(panelOeste, BorderLayout.WEST);
	}

	public JPanel getPanelCentro() {
		return panelCentro;
	}

	public void setPanelCentro(PanelViaje panelCentro) {
		this.panelCentro = panelCentro;
		this.frame.add(panelCentro, BorderLayout.CENTER);
	}

    @Override
    public void update() {
        panelCentro.actualizarCiudades();
        frame.getContentPane().repaint();
    }

}
