package view;

import java.util.Collection;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import userInterface.TravelPlanner;

public class PanelPlanificadores extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private Collection<String> planificadores;
	private JComboBox<String>  comboBox;
	private JButton            btnEnviar;
	
	public PanelPlanificadores(TravelPlanner model) {
		planificadores = model.getPlanificadores();
		inicializarComponentes();
	}
	
	private void inicializarComponentes() {
		btnEnviar = new JButton("Enviar");
		comboBox  = new JComboBox<String>();
		actualizarComponentes();
	}
	
	public void actualizarComponentes() {
	    removeAll();
	    comboBox.removeAllItems();
	    planificadores.forEach(comboBox::addItem);
	    add(comboBox);
	    add(btnEnviar);
	}
	
	public void setComboBox(JComboBox<String> combo) {
		this.comboBox = combo;
	}
	
	public JComboBox<String> getComboBox() {
		return this.comboBox;
	}
	
	public String getPlanificadorSeleccionado() {
	    return (String) comboBox.getSelectedItem();
	}

	public JButton getBtnEnviar() {
		return btnEnviar;
	}

	public void setBtnEnviar(JButton btnEnviar) {
		this.btnEnviar = btnEnviar;
	}

}
